
// Екранування використовується для того щоб щось знайти наприклад символ 
// Є дефолтний визов фунцій через function , ф ще є анонімні функції 
// Hoisting (підняття), воно використовується для читання всього коду. Зі зміними він працює добре, наприклад 
// num 6
// let num
// в цьому випадку помилку помилку не видасть так як num ініціалізували нижче, тоді javascript піднімається наверх щоб знайти число що присвоєне цій змінній






function createNewUser() {

    const newUser = {}
    const newFirstName = prompt("Введи имя");
    const newLastName = prompt("Введи фамилию");
    const newUserDate = prompt("Введи дату нарождения(в форматі dd.mm.yyyy)", 'dd.mm.yyyy');



    newUser.firstName = newFirstName;
    newUser.lastName = newLastName;
    newUser.birthday = new Date (
        newUserDate.slice(6,10),
        newUserDate.slice(3,5) - 1,
        newUserDate.slice(0,2)
    );


    newUser.getLogin = function () {
        return this.firstName[0].toLowerCase() + this.lastName.toLowerCase()
    }


    newUser.getAge = function () {
        return (
            Math.floor(
                (new Date().getTime()- new Date(this.birthday)) / (24 * 3600 * 365.25 * 1000)
            ) + " Years old"
        );
    };


    newUser.getPassword = function () {
        return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.getFullYear() 
    };




    console.log(newUser.getLogin());
    console.log(newUser.getPassword());
    console.log(newUser.getAge());
    
    return newUser;
}



createNewUser()