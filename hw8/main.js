
// 1) DOM це дії з html через js, тобто надання динаміки коду або ж повністю створення сторінки не викоритовуючи html. 
//
// 2) innerHtml працює з дочірніми елементами а innerText ні, також він підтягує CSS стилі до елемента
//
// 3) document.getElementById() document.getElementByTagName document.getElementByClassName() document.querySelector() document.querySelectorAll()
//  Для мене найкращий document.querySelector()
//



// 1

const paragraph = document.querySelectorAll('p');

for (let elem of paragraph) {
    elem.style.backgroundColor = '#ff0000'
  }

// 2

const optionItem = document.querySelector('#optionsList');

const parentOption = optionItem.parentElement;

const childOption = optionItem.childNodes;

for (const elem of childOption) {
    console.log(elem);
    console.log(`ChildNode ${elem} and type ${typeof elem}`);
    
}

console.log(parentOption);
console.log(optionItem);


// 3


const test = document.createElement('p');

test.className = 'testParagraph';

test.innerHTML = 'This is a paragraph';

console.log(test);




// 4 

const mainHeader = document.querySelector('.main-header');

const childHeader = mainHeader.childNodes;


for (const elem of childHeader) {
    elem.className += ' nav-item';
    
}

console.log(childHeader);


// 5

const sectionTitle = document.querySelectorAll('.section-title');

for (const elem of sectionTitle) {
    elem.classList.remove('section-title')
    console.log(elem);
}
